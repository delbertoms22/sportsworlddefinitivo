//
//  LoginViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 26/12/17.
//  Copyright © 2017 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import CryptoSwift
import SwiftyJSON
class LoginViewController: UIViewController {
    
    var player: AVPlayer?
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var labelPolitica: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPolitica.text = "Al iniciar sesión aceptas la Política de Privacidad y los Términos de uso de \n Sports World." 
        loginButton.layer.cornerRadius = 8 
        activity.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadVideo()
    }
    
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //MARK: - MÉTODOS
    //DECLARA TODOS LOS MÉTODOS.
    
    func validateTextFields () -> Bool {
        if userLogin != "" && passwordLogin != ""{
            Alert.ShowAlert(title: "", message: "Verifica que tus campos esten llenos antes de continuar", titleForTheAction: "ACEPTAR", in: self)
            return false
        } else {
            return true
        }
    }
    func passToAnotherView() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        self.present(vc, animated: true, completion: nil)

    }
  
    //REALIZA EL LOGIN
    
    func login() {
        self.activity.isHidden = false
        self.activity.startAnimating()
        view.isUserInteractionEnabled = false
        let passSha1 = passwordLogin.sha1()
        let stringToLowerCase = "\(userLogin!).\(passSha1)".lowercased()
        let stringToBase64 = stringToLowerCase.toBase64()
        print("ESte es el stringToBase64", stringToBase64)
      
        APIManager.sharedInstance.login(authKey: stringToBase64 , onSuccess: { json in
          
               DispatchQueue.main .async {

            if APIManager.sharedInstance.status == true {
                
                APIManager.sharedInstance.getClubesForRegister(onSuccess: { json in
                    
                    DispatchQueue.main.async {
                        self.activity.isHidden = true
                        self.activity.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                        self.passToAnotherView()
                    }
                }, onFailure: { error in
                      Alert.ShowAlert(title: "", message: "Lo sentimos, hubo un error intenta mas tarde.", titleForTheAction: "Aceptar", in: self)
                })
                
            } else if APIManager.sharedInstance.status == false {
                if(json == JSON.null){
                    Alert.ShowAlert(title: "", message: "Lo sentimos, hubo un error intenta mas tarde.", titleForTheAction: "Aceptar", in: self)
                }else{
                self.activity.stopAnimating()
                self.activity.isHidden = true
                self.view.isUserInteractionEnabled = true
              Alert.ShowAlert(title: "", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                self.activity.stopAnimating()
                self.activity.isHidden = true
                }

            }
            }
            //self.activity.isHidden = true

        }, onFailure: { error in
            print("Error en hacer este login")
            self.activity.stopAnimating()
             self.activity.isHidden = true
            self.view.isUserInteractionEnabled = true
            Alert.ShowAlert(title: "", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
        })
      //self.activity.isHidden = true
   
    }
    //NO HACE NADA
    @objc func passToAnotherController() {
        
    }
    
    //CONFIGURA EL TIMER DEL VIDEO
    func schedule() {
        _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(passToAnotherController), userInfo: nil, repeats: true);
        
    }
  
    //CONFIGURA EL VIDEO.
    private func loadVideo() {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch { }
        
        let path = Bundle.main.path(forResource: "VIDEO_LOGIN", ofType:"mp4")
        player = AVPlayer.init(url: NSURL(fileURLWithPath: path!) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        player?.play()
        self.view.layer.addSublayer(playerLayer)
        schedule()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { _ in
            self.player?.seek(to: kCMTimeZero)
            self.player?.play()
        }
    }
/////////////////////////////////FUNCIONES PARA MOSTRAR/OCULTAR ACTIVITY/////////////////////////////////////////
   
    
    //MARK: - BOTONES
    //CONFIGURA EL COMPORTAMIENTO DE LOS BOTONES.
    
    @IBAction func clickLoginButton(_ sender: Any) {
        
        if Reachability.isConnectedToNetwork() {
             login()
        } else{
            Alert.ShowAlert(title: "", message: "No cuentas con internet, verifica tu conexión o intenta más tarde.", titleForTheAction: "Aceptar", in: self)
        }
      
        
    }
  
    
}
