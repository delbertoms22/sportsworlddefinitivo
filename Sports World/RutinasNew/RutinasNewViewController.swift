//
//  RutinasNewViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/20/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class RutinasNewViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var rutinas = ["Dance","Wellness","Strength","Intensity","Contact","Kids"]
    var descripcionRutinas = ["uno","dos","tres","cuatro","cinco","seis","siete","ocho"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.layer.borderWidth = 0.5
        searchBar.layer.cornerRadius = 20
        searchBar.searchBarStyle = .minimal
        searchBar.layer.borderColor = UIColor.red.cgColor
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rutinas.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cel = tableView.dequeueReusableCell(withIdentifier: "cellTable", for: indexPath as IndexPath) as! RutinasNewTableViewCell
        
        return cel
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension RutinasNewViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return descripcionRutinas.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollection", for: indexPath as IndexPath) as! RutinasNewCollectionViewCell
          cel.roundedView.layer.cornerRadius = 15
          cel.roundedView.layer.borderWidth = 1
          cel.roundedView.layer.borderColor = UIColor.red.cgColor
          cel.descripcionRu.text = descripcionRutinas[indexPath.row]
        
        return cel
    }
}
