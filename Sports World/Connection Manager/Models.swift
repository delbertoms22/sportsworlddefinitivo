//
//  Models.swift
//  Sports World
//
//  Created by Glauco Valdes on 6/11/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import Foundation
open class Club{
    var name : String = ""
    var group : String = ""
    var clubId : Int = 0
    var distance : Double = 0.0
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var agenda : Int = 0
    var tele : String = ""
    var idEstado : Int = 0
    var address : String = ""
    var schedule : String = ""
    var clave : String = ""
    var dCount : Int = 0
    var ruta360 : String = ""
    var rutaVideo : String = ""
    var preventa : Int = 0
    var estado : String = ""
}

open class ClassSW{
    var idinstalacionactividadprogramada : Int = 0
    var club : String = ""
    var salon : String = ""
    var idsalon : Int = 0
    var clase : String = ""
    var idclase : Int = 0
    var instructor : String = ""
    var inicio : String = ""
    var fin : String = ""
    var iniciovigencia : String = ""
    var finvigencia : String = ""
    var capacidadideal : Int = 0
    var capacidadmaxima : Int = 0
    var capacidadregistrada : Int = 0
    var reservacion : Int = 0
    var confirmados : Int = 0
    var agendar : Int = 0
    var demand : Int = 0
    var inscrito : Bool = false
}
open class GenericResponse{
    var status : Bool = false
    var message : String = ""
}

open class DescriptionResponse{
    var status : Bool = false
    var message : String = ""
    var data : DescriptionData = DescriptionData()
}
open class DescriptionData{
    var idClass : Int = 0
    var descripcionHTML : String = ""
    var clave : String = ""
    
}
open class ClassResponse{
    var status : Bool = false
    var message : String = ""
    var data : [ClassSW] = [ClassSW]()
}
open class ClassCategoriaResponse{
    var status : Bool = false
    var message : String = ""
    var data : [ClassCategoria] = [ClassCategoria]()
}
open class ClassCategoria{
    var idinstalacionactividadprogramada : Int = 0
    var club : String = ""
    var salon : String = ""
    var idsalon : Int = 0
    var clase : String = ""
    var idclase : Int = 0
    var instructor : String = ""
    var inicio : String = ""
    var fin : String = ""
    var iniciovigencia : String = ""
    var finvigencia : String = ""
    var capacidadideal : Int = 0
    var capacidadmaxima : Int = 0
    var capacidadregistrada : Int = 0
    var reservacion : Int = 0
    var confirmados : Int = 0
    var agendar : Int = 0
    var demand : Int = 0
    var inscrito : Bool = false
}
open class ClubResponse{
    var status : Bool = false
    var message : String = ""
    var data : ClubData = ClubData()
}
open class AsignMetaResponse{
    var code : Int = 0
    var message : String = ""
    var data : [DietDateData] =  [DietDateData]()
}

open class AsignInbodyResponse{
    var code : Int = 0
    var message : String = ""
    
}

open class MetasResponse{
    var code : Int = 0
    var message : String = ""
    var data : [MetaData] = [MetaData]()
}
open class DietDateData{
    var usuario : Int = 0
    var dia : Date = Date()
    var cableMeta : Int = 0
    var nombreMeta : String = ""
    var cableDieta : Int = 0
    var nombreDieta : String = ""
}
open class MetaData{
    var id : Int = 0
    var nombre : String = ""
}

open class ComidaResponse{
    var code : Int = 0
    var message : String = ""
    var data : [Comida] = [Comida]()
}
open class Comida{
    var tipo : String = ""
    var descripcion : String = ""
    var horario : String = ""
}
open class RecetaResponse{
    var code : Int = 0
    var message : String = ""
    var data : [Receta] = [Receta]()
}

open class Receta{
    var id : Int = 0
    var nombre : String = ""
    var descripcion : String = ""
    var foto : String = ""
}
open class LastInbodyResponse {
    var status = ""
    var message = ""
    var data : [Double] = [Double]()
}
open class LastInbody{
    var peso: Double = 0.0
    var estatura :  Double = 0.0
    var RCC :  Double = 0.0
    var PGC :  Double = 0.0
    var IMC :  Double = 0.0
    var MME : Double = 0.0
    var MCG :  Double = 0.0
    var ACT :  Double = 0.0
    var minerales :  Double = 0.0
    var proteina :  Double = 0.0
    var fcresp :  Double = 0.0
}

open class ClubData{
    var latitud : Double = 0
    var agendar : Int = 0
    var favorito : Bool = false
    var direccion : String = ""
    var horario : String = ""
    var idun : Int = 0
    var imagenes_club : [String] = [String]()
    var ruta360 : String = ""
    var nombre : String = ""
    var rutavideo : String = ""
    var preventa : Int = 0
    var telefono : String = ""
    var longitud : Double = 0
}
open class FacturaResponse{
    var code : Int = 0
    var message : String = ""
    var data : [Factura] = [Factura]()
}
open class Factura{
    var folio : String = ""
    var importe : String = ""
    var fecha : String = ""
    var pdf : String = ""
}

open class EntrenamientosResponse {
  var code: Int = 0
  var message: String = ""
    var data: [Entrenamiento] = [Entrenamiento]()
}

open class RutinasResponse {
    var code: Int = 0
    var message: String = ""
    var fechaInicio: Date = Date()
    var fechaFin: Date = Date()
}
open class Entrenamiento {
    var clave: String = ""
    var orden: Int = 0
    var nombre: String = ""
    var video: String = ""
    var series: Int = 0
    var repeticiones: Int = 0
    var descanso: Int = 0
    var completado: Bool = false
    var fechaStr: String = ""
    
}
open class RutinaResponse {
    var code: Int = 0
    var message: String = ""
    var data: [Rutina] = [Rutina]()
}

open class CompletadoResponse {
    var code: Int = 0
    var message: String = ""
    var completado: Bool = false
}
open class Rutina {
    var clave: String = ""
    var orden: Int = 0
    var nombre: String = ""
    var video: String = ""
    var series: Int = 0
    var repeticiones: Int = 0
    var descanso: Int = 45
    var completado: Bool = false 
}
 open class Horarios {
    var hora:[String:Int]
    var fecha:String
    init(hora:[String:Int], fecha:String) {
        self.hora = hora
        self.fecha = fecha
    }
    
}

open class DatosResponse {
    var code : Int = 0
    var message : String =  ""
    var data : [Datos] = [Datos]()
}
open class Datos {
     var idPersona: Int = 0
    var nivel : Int = 0
    var rutina : String = ""
   
}

open class MenuActividadResponse {
    var code : Int = 0
    var message : String =  ""
    var data : [MenuActividad] = [MenuActividad]()
}
open class MenuActividad {
    var idPersona: Int = 0
    var nivel : Int = 0
    var rutina : String = ""
    
}

open class PasesResponse {
  
    var data : [Pases] = [Pases]()
}
open class Pases {
 
     var producto : String =  ""
     var cuando : String = ""
  
}
open class PasesPorAsignarResponse {
    
    var data : [PasesPorAsignar] = [PasesPorAsignar]()
}
open class PasesPorAsignar {
    var nombre : String = ""
    var finVigencia : String = ""
}

open class RutinaCasaResponse {
    var code : Int = 0
    var message : String =  ""
    var data : [RutinasCasa] = [RutinasCasa]()
}

open class RutinasCasa {
    var usuario: String = ""
    var nombreEntrenamiento: String = ""
    var claveEntrenamiento: Int = 0
    var calificacion: Int = 0
    var fechaReproduccion: String = ""
    var foto: String = ""
    var video: String = ""
    var categoria: String = ""
    var numeroReproduccion: Int = 0
    
}
open class ClaseCategoriasResponse {
    var status : Bool = false 
    var message : String =  ""
    var data : [ClaseCategorias] = [ClaseCategorias]()
    var clases :[Clases] = [Clases]()
}
open class ClaseCategorias {
    var categoria: String = ""
    var id: Int = 0
    
}
open class Clases {
    var clase : String = ""
    var idClase : Int = 0
}
open class EventosResponse {
   var code : Int = 0
   var message : String = ""
    var data : [Eventos] = [Eventos]()
}
open class Eventos {
     var id: Int = 0
     var nombre : String = ""
    var ubicacion : String = ""
    var descripcion : String = ""
    var imagen : String = ""
    var fecha : String = ""
    var galeria : [GaleriaEventos] = [GaleriaEventos]()
}
open class GaleriaEventos {
    var idFoto : Int = 0
    var ruta : String =  ""
}

