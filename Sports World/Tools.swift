//
//  Tools.swift
//  Sports World
//
//  Created by Glauco Valdes on 6/11/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import Foundation

extension Array {
    func removingDuplicates<T: Hashable>(byKey key: (Element) -> T)  -> [Element] {
        var result = [Element]()
        var seen = Set<T>()
        for value in self {
            if seen.insert(key(value)).inserted {
                result.append(value)
            }
        }
        return result
    }
}
extension String {
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
}
func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}
open class Tools{
    static func getHash()->String{
        
        let date = Date()
        let calendar = Calendar.current
        
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let day = calendar.component(.day, from: date)
        
        let timestamp : Double = makeDate(year: year, month: month, day: day, hr: 0, min: 0, sec: 0  ).timeIntervalSince1970
        
        
        return String(String(format: timestamp == floor(timestamp) ? "%.0f" : "%.1f", timestamp) + "#sportsworldMX$")
    }
    static    func makeDate(year: Int, month: Int, day: Int, hr: Int, min: Int, sec: Int) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        // calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min, second: sec)
        return calendar.date(from: components)!
    }
    
}

extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

extension UIImageView {
    public func downloadImage(downloadURL : String, completion: @escaping (Bool?) -> ()) {
        
        
        if (self.image == nil){
            self.image = UIImage(named: "profileDefault")
        }
        //let imageSufix =  "profile-" + userAppfterId
        //self.image = UIImage(named: "profileDefault")
        if(downloadURL != ""){
            URLSession.shared.dataTask(with: URL(string: downloadURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!, completionHandler: { (data, response, error) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse , httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType , mimeType.hasPrefix("image"),
                let data = data , error == nil,
                let imageLocal = UIImage(data: data)
                else {
                    
                    
                    /*let fbUrl = "https://graph.facebook.com/" + fbId + "/picture?type=large"
                     
                     URLSession.shared.dataTask(with: URL(string: fbUrl)!, completionHandler: { (data, response, error) -> Void in
                     guard
                     let httpURLResponse = response as? HTTPURLResponse , httpURLResponse.statusCode == 200,
                     let mimeType = response?.mimeType , mimeType.hasPrefix("image"),
                     let data = data , error == nil,
                     let imageLocal = UIImage(data: data)
                     else { completion(true)
                     return }
                     DispatchQueue.main.async { () -> Void in
                     self.image = imageLocal
                     completion(true)
                     }
                     }).resume()*/
                    
                    completion(true)
                    return
            }
            
            
            DispatchQueue.main.async { () -> Void in
                
                self.image =  imageLocal
                completion(true)
                
                
                
                
            }
        }).resume()
        }else{
            completion(true)
        }
        
    }
}
extension URLRequest {
    
    private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")
        
        return string
            .addingPercentEncoding(withAllowedCharacters: characterSet)!
            .replacingOccurrences(of: " ", with: "+")
            .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func encodeParameters(parameters: [String : String]) {
        httpMethod = "POST"
        
        let parameterArray = parameters.map { (arg) -> String in
            let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
        
        httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as! UIViewController?
            }
        }
        return nil
    }
}

extension Collection where Iterator.Element == Any {
    var doubleArrayFromStrings: [Double] {
        return compactMap{ Double($0 as? String ?? "") }
    }
}

extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
     }
}
extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        return Dictionary.init(grouping: self, by: key)
    }
}
