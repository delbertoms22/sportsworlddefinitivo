//
//  EventosTableViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 8/6/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class EventosTableViewCell: UITableViewCell {
    @IBOutlet weak var nameEventoLabel: UILabel!
    @IBOutlet weak var lugarEventoLabel: UILabel!
    @IBOutlet weak var fechaEventoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
