//
//  EventosMainViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 8/6/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class EventosMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    private let cell = "EventosTableViewCell"
    var eventos =  [Eventos]()
    var galeria = [GaleriaEventos]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.isHidden = true 
        segmentedControl.selectedSegmentIndex = 0
        
        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)

        
        self.activity.isHidden = true
      
        tableView.delegate = self
        tableView.dataSource = self
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont(name: "LarkeNeue-Regular", size: 23)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 1
        let firstAttr = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //attributes for the second part of the string
        
        //initializing the attributed string and appending the two parts together
        let attrString = NSMutableAttributedString(string: "Eventos", attributes: firstAttr)
        //setting the attributed string as an attributed text
        titleLabel.attributedText = attrString
        
        //finding the bounds of the attributed text and resizing the label accordingly
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)
        titleLabel.frame.size = attrString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
        
        //setting the label as the title view of the navigation bar
        navigationItem.titleView = titleLabel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        APIManager.sharedInstance.getEventos(onSuccess: { response in
            self.eventos = []
            self.activity.isHidden = false
            self.activity.startAnimating()
            self.view.isUserInteractionEnabled = false
            DispatchQueue.main.async {
                
               
                if response.code == 200 {
                  self.eventos = response.data
                     self.tableView.reloadData()
                self.activity.isHidden = true
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                
                }else{
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                      self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "", message: response.message, titleForTheAction: "Aceptar", in: self)
                    self.view.isUserInteractionEnabled = true
                }
                self.activity.isHidden = true
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
            }
        })
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
    
        return self.eventos.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! EventosTableViewCell
        cel.lugarEventoLabel.text! = self.eventos[indexPath.row].ubicacion
        cel.nameEventoLabel.text! = self.eventos[indexPath.row].nombre
        cel.fechaEventoLabel.text! = self.eventos[indexPath.row].fecha
        return cel 
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! EventosTableViewCell
        
          let vc = storyboard?.instantiateViewController(withIdentifier: "DetalleEventoViewController") as? DetalleEventoViewController
         vc?.tituloEvento = self.eventos[indexPath.row].nombre
        vc?.descripcionEvento = self.eventos[indexPath.row].descripcion
        
   
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    @IBAction func clickSegmentedControl(_ sender: Any) {
        if (sender as AnyObject).selectedSegmentIndex == 0 {
            containerView.isHidden = true
            tableView.isHidden = false
        } else if (sender as AnyObject).selectedSegmentIndex == 1 {
            containerView.isHidden = false
            tableView.isHidden = true
        }
    }
    
}
