//
//  CalendarioInBodyViewController.swift
//  Sports World
//
//  Created by VicktorManuel on 7/7/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit


import UIKit
import JTAppleCalendar
import SwiftyJSON

class CalendarioInBodyViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var moveIndex : Int = 0
    var year : Int = 0
    var month : Int = 0
    var countCells = 0
    var dateSelected = ""
    
    @IBOutlet weak var enviarButton: UIButton!
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        var date = Date()
        var calendar = Calendar.current
        
        self.year = calendar.component(.year, from: date)
        self.month = calendar.component(.month, from: date)
        
        
        let dateComponents = DateComponents(year: self.year, month: self.month)
        calendar = Calendar.current
        date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        let cal = Calendar.current
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var dateComponentsTemp = DateComponents()
        dateComponentsTemp.year = self.year
        dateComponentsTemp.month = self.month
        dateComponentsTemp.day = 1
        
        
        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponentsTemp)
        
        self.moveIndex = cal.component(.weekday, from: someDateTime!) - 1
        
        self.countCells = numDays + self.moveIndex
        return self.countCells
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! CalendarCollectionViewCell
        cel.dayLabel.textColor = UIColor.black
        if(indexPath.row >= moveIndex){
            cel.dayLabel.text = "\(indexPath.row - moveIndex + 1)"
            
            var dateComponentsTemp = DateComponents()
            dateComponentsTemp.year = self.year
            dateComponentsTemp.month = self.month
            dateComponentsTemp.day = indexPath.row - moveIndex + 1
            
            // Create date from components
            let userCalendar = Calendar.current // user calendar
            let someDateTime = userCalendar.date(from: dateComponentsTemp)
            let formatter = DateFormatter()
            
            formatter.dateFormat = "yyyy-MM-dd"
            let fechaTem : String = formatter.string(from: someDateTime!)
            if(self.arregloFechas.filter({$0 == fechaTem}).count > 0){
                cel.dayLabel.textColor = UIColor.white
            }
            
            
        }else{
            cel.dayLabel.text = ""
        }
        
        cel.dayLabel.layer.cornerRadius = cel.dayLabel.bounds.width / 2
        cel.dayLabel.layer.borderWidth = 2
        return cel
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        /*let yearString = "2018"
        let montOfYear = "8"
        
        guard let year = Int(yearString), let weekOfYear = Int(weekOfYearString) else {return}
        let components = DateComponents(weekOfYear: weekOfYear, yearForWeekOfYear: year)
        guard let date = Calendar.current.date(from: components) else {return}
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let outputDate = df.string(from: date)*/
        for i in 0...(self.countCells - 1) {
            // do something
            let cell : CalendarCollectionViewCell = self.calendarioAmor.cellForItem(at:  IndexPath(row: i, section: 0)) as! CalendarCollectionViewCell
            cell.dayLabel.layer.borderColor = UIColor.clear.cgColor
        }
        let tempCellSelect : CalendarCollectionViewCell = self.calendarioAmor.cellForItem(at: indexPath) as! CalendarCollectionViewCell
        
        var dateComponentsTemp = DateComponents()
        dateComponentsTemp.year = self.year
        dateComponentsTemp.month = self.month
        dateComponentsTemp.day = indexPath.row - moveIndex + 1
        
        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponentsTemp)
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        let fechaTem : String = formatter.string(from: someDateTime!)
        if(self.arregloFechas.filter({$0 == fechaTem}).count > 0){
            tempCellSelect.dayLabel.layer.borderColor = UIColor.white.cgColor
            self.dateSelected = fechaTem
        }
        
        
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print((calendarioAmor.frame.width)/7)
        return CGSize(width:(calendarioAmor.frame.width)/7, height: (calendarioAmor.frame.width)/7)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @IBOutlet weak var nueve: UIButton!
    @IBOutlet weak var cinco: UIButton!
    @IBOutlet weak var doce: UIButton!
    var tempClub : Club = Club()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerClubes .count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerClubes[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentClub = self.pickerClubes[row]
        //APIManager.sharedInstance.selectedClub = self.currentClub
        self.clubName.text = self.currentClub.name
        //APIManager.sharedInstance.infoVC.updateValues()
        self.horarioSeleccionado = ""
        self.dateSelected = ""
        
        self.cinco.layer.cornerRadius = self.cinco.bounds.height / 2
        self.cinco.layer.borderWidth = 2
        self.cinco.layer.borderColor = UIColor.white.cgColor
        self.cinco.setTitleColor(UIColor.white, for: .normal)
        self.cinco.backgroundColor = UIColor.clear
        
        self.nueve.layer.cornerRadius = self.cinco.bounds.height / 2
        self.nueve.layer.borderWidth = 2
        self.nueve.layer.borderColor = UIColor.white.cgColor
        self.nueve.setTitleColor(UIColor.white, for: .normal)
        self.nueve.backgroundColor = UIColor.clear
        
        self.doce.layer.cornerRadius = self.cinco.bounds.height / 2
        self.doce.layer.borderWidth = 2
        self.doce.layer.borderColor = UIColor.white.cgColor
        self.doce.setTitleColor(UIColor.white, for: .normal)
        self.doce.backgroundColor = UIColor.clear
        
        
        UIView.animate(withDuration: 1.0, animations: {
            self.picker.alpha = 0
        }) { (finished) in
            self.picker.isHidden = true
            self.view.endEditing(true)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.lblTapped(sender:)))
            self.clubName.isUserInteractionEnabled = true
            self.clubName.addGestureRecognizer(tap)
            self.updateCalendar()
            
        }
    }
    
    @IBAction func lblTapped(sender: UITapGestureRecognizer) {
        print("*")
        picker.isHidden = false
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
          return NSAttributedString(string: self.pickerClubes[row].name, attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
    }
    let formatter = DateFormatter()
    var arregloFechas:[String] = [String]()
    var itemsHorario = [[String:[String:Int]]]()
    var cambiarColor:Bool = false
    var currentClub : Club = Club()
    
    @IBOutlet weak var clubName: UILabel!
    //Picker
    var pickerClubes : [Club] = [Club]()
    var picker = UIPickerView()
    
    //Selecciona Horario
    var horarioSeleccionado = ""
    //Fecha solicitud
    var fechaSeleccionada = "2018-11-30"
    
    @IBAction func picker(_ sender: Any) {
        print("*")
        self.picker.alpha = 1
        picker.isHidden = false
    }
    
    @IBAction func backClick(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func enviarSolicitud(_ sender: Any) {
        
        if(self.horarioSeleccionado != "" && self.dateSelected != ""){
            APIManager.sharedInstance.hacerReservacion(horario: self.horarioSeleccionado, fechaSolicitud: self.dateSelected, idClub: self.currentClub.clubId, onSuccess: { json in
            DispatchQueue.main.async {
                if(json.code == 200){
             Alert.ShowAlert(title: "", message: json.message, titleForTheAction: "Aceptar", in: self)
                    let _ = self.navigationController?.popViewController(animated: true)
                }else{
             Alert.ShowAlert(title: "", message: json.message, titleForTheAction: "Aceptar", in: self)
                }
            }
            
        })
        }else{
            Alert.ShowAlert(title: "", message: "Selecciona una fecha y horario", titleForTheAction: "Aceptar", in: self)
        }
    }
    
    @IBOutlet weak var botonEnviar: UIButton!
    
    @IBOutlet weak var calendarioAmor: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enviarButton.layer.cornerRadius = 8
        
        self.currentClub = (APIManager.sharedInstance.allClubes.sorted(by: { $0.distance < $1.distance }).first)!
        //APIManager.sharedInstance.selectedClub = self.currentClub
       
        
        let tempclubes = APIManager.sharedInstance.allClubes.sorted(by: { $0.distance < $1.distance })
        var tempList = [Club]()
        for temp in tempclubes{
            
            if(tempList.filter({$0.name.uppercased() == temp.name.uppercased()}).count == 0){
                tempList.append(temp)
            }else if(tempList.count == 0){
                tempList.append(temp)
            }
        }
        self.pickerClubes = tempList.filter({$0.group.uppercased() != "FAVORITOS"}).filter({$0.group.uppercased() != "PRÓXIMAS APERTURAS"}).sorted(by: {$0.name.localizedCaseInsensitiveCompare($1.name) == ComparisonResult.orderedAscending })
        
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.dismissKeyboard))
        //view.addGestureRecognizer(tap)
        
        let pickerRect = CGRect(x: 0, y: 64, width: self.view.bounds.width, height: self.view.bounds.height - 64)
        
        
        picker.frame = pickerRect
        picker.backgroundColor = UIColor.black
        picker.setValue(UIColor.white, forKeyPath: "textColor")
        
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = true
        view.addSubview(picker)
        // Do any additional setup after loading the view, typically from a nib.
        self.cinco.layer.cornerRadius = self.cinco.bounds.height / 2
        self.cinco.layer.borderWidth = 2
        self.cinco.layer.borderColor = UIColor.white.cgColor
        
        self.nueve.layer.cornerRadius = self.cinco.bounds.height / 2
        self.nueve.layer.borderWidth = 2
        self.nueve.layer.borderColor = UIColor.white.cgColor
        
        self.doce.layer.cornerRadius = self.cinco.bounds.height / 2
        self.doce.layer.borderWidth = 2
        self.doce.layer.borderColor = UIColor.white.cgColor
        
        self.calendarioAmor.delegate = self
        self.calendarioAmor.dataSource = self
        
        updateCalendar()
    }
    var horariosTemp : [Horarios] = [Horarios]()
    var horariosToShow : [Horarios] = [Horarios]()
    func updateCalendar(){
        if(self.countCells > 0){
            for i in 0...(self.countCells - 1) {
                // do something
                let cell : CalendarCollectionViewCell = self.calendarioAmor.cellForItem(at:  IndexPath(row: i, section: 0)) as! CalendarCollectionViewCell
                cell.dayLabel.layer.borderColor = UIColor.clear.cgColor
            }
        }
        self.arregloFechas = [String]()
        self.calendarioAmor.reloadData()
        
        APIManager.sharedInstance.getReservacionesInBody(clubId: self.currentClub.clubId,onSuccess: { json in
            DispatchQueue.main.async {
                if (json == JSON.null) {
                    print("Hubo un error")
                }else{
                    for fechas in APIManager.sharedInstance.horarios{
                        print(fechas.fecha)
                        //self.arregloFechas.append(fechas.fecha)
                        self.horariosTemp = APIManager.sharedInstance.horarios
                    }
                    print("Arreglo fechas:\(self.arregloFechas.count)")
                    DispatchQueue.main.async {
                        //self.cambiarColor = true
                        self.calendarioAmor.reloadData()
                    }
                    
                }
                
            }
        }, onFailure: { error in
            
        })
    }
    
    func configuracionCalendarioDeAmor(){
        //calendarioAmor.minimumLineSpacing = 0
        //calendarioAmor.minimumInteritemSpacing = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    @IBAction func sieteadoce(_ sender: Any) {
        self.horarioSeleccionado = "h7-12"
        
        self.cinco.layer.cornerRadius = self.cinco.bounds.height / 2
        self.cinco.layer.borderWidth = 2
        self.cinco.layer.borderColor = UIColor.white.cgColor
        self.cinco.setTitleColor(UIColor.white, for: .normal)
        self.cinco.backgroundColor = UIColor.clear
        
        self.nueve.layer.cornerRadius = self.cinco.bounds.height / 2
        self.nueve.layer.borderWidth = 2
        self.nueve.layer.borderColor = UIColor.white.cgColor
        self.nueve.setTitleColor(UIColor.white, for: .normal)
        self.nueve.backgroundColor = UIColor.clear
        
        self.doce.layer.cornerRadius = self.cinco.bounds.height / 2
        self.doce.layer.borderWidth = 2
        self.doce.layer.borderColor = UIColor.white.cgColor
        self.doce.setTitleColor(UIColor.black, for: .normal)
        self.doce.backgroundColor = UIColor.white
        
        self.reloadCalendar()
    }
    @IBAction func cincoanueve(_ sender: Any) {
        self.horarioSeleccionado = "h17-21"
        
        self.cinco.layer.cornerRadius = self.cinco.bounds.height / 2
        self.cinco.layer.borderWidth = 2
        self.cinco.layer.borderColor = UIColor.white.cgColor
        self.cinco.setTitleColor(UIColor.white, for: .normal)
        self.cinco.backgroundColor = UIColor.clear
        
        self.nueve.layer.cornerRadius = self.cinco.bounds.height / 2
        self.nueve.layer.borderWidth = 2
        self.nueve.layer.borderColor = UIColor.white.cgColor
        self.nueve.setTitleColor(UIColor.black, for: .normal)
        self.nueve.backgroundColor = UIColor.white
        
        self.doce.layer.cornerRadius = self.cinco.bounds.height / 2
        self.doce.layer.borderWidth = 2
        self.doce.layer.borderColor = UIColor.white.cgColor
        self.doce.setTitleColor(UIColor.white, for: .normal)
        self.doce.backgroundColor = UIColor.clear
        
        self.reloadCalendar()
    }
    
    @IBAction func doceacinco(_ sender: Any) {
        self.horarioSeleccionado = "h12-17"
        
        self.cinco.layer.cornerRadius = self.cinco.bounds.height / 2
        self.cinco.layer.borderWidth = 2
        self.cinco.layer.borderColor = UIColor.white.cgColor
        self.cinco.setTitleColor(UIColor.black, for: .normal)
        self.cinco.backgroundColor = UIColor.white
        
        self.nueve.layer.cornerRadius = self.cinco.bounds.height / 2
        self.nueve.layer.borderWidth = 2
        self.nueve.layer.borderColor = UIColor.white.cgColor
        self.nueve.setTitleColor(UIColor.white, for: .normal)
        self.nueve.backgroundColor = UIColor.clear
        
        self.doce.layer.cornerRadius = self.cinco.bounds.height / 2
        self.doce.layer.borderWidth = 2
        self.doce.layer.borderColor = UIColor.white.cgColor
        self.doce.setTitleColor(UIColor.white, for: .normal)
        self.doce.backgroundColor = UIColor.clear
        
        self.reloadCalendar()
    }
    
    func reloadCalendar(){
        self.arregloFechas = [String]()
        if(self.countCells > 0){
            for i in 0...(self.countCells - 1) {
                // do something
                let cell : CalendarCollectionViewCell = self.calendarioAmor.cellForItem(at:  IndexPath(row: i, section: 0)) as! CalendarCollectionViewCell
                cell.dayLabel.layer.borderColor = UIColor.clear.cgColor
            }
        }
        //self.calendarioAmor = newc
        self.calendarioAmor.reloadData()
        for hourTemp in self.horariosTemp{
            if(hourTemp.hora[self.horarioSeleccionado]! > 0){
                self.arregloFechas.append(hourTemp.fecha)
            }
        }
        self.calendarioAmor.reloadData()
        //self.calendarioAmor.selectedDates
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}




